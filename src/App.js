import "./App.css";
import {
  Card,
  Container,
  Row,
  Form,
  Button,
  Alert,
  Spinner,
} from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import { useState } from "react";
import { WiCloudy, WiRainMix, WiDaySunny } from "react-icons/wi";

function App() {
  const [city, setCity] = useState(null);
  const [showAlert, setShowAlert] = useState(false);
  const [searchInput, setSearchInput] = useState("");
  const [loading, setLoading] = useState("");

  function weatherIcon(weather) {
    // return an icon for the corresponding weather type
    if (weather === "zonnig") {
      return <WiDaySunny />;
    } else if (weather === "regen") {
      return <WiRainMix />;
    } else if (weather === "bewolkt") {
      return <WiCloudy />;
    }
  }

  function search() {
    // while the app is fetching data, show a spinner
    setLoading(true);
    fetch("https://adsd-weather-api.herokuapp.com/weather?city=" + searchInput)
      .then((res) => {
        // check if response status is OK (200)
        // if not, throw error
        if (res.status === 200) {
          return res.json();
        } else {
          throw new Error();
        }
      })
      .then((json) => {
        // clear search input
        // set city object
        // and hide alert if needed
        setSearchInput("");
        setCity(json);
        setShowAlert(false);
      })
      .catch((err) => {
        // there was an error
        // remove the city object
        // and show the error alert
        setSearchInput("");
        setCity(null);
        setShowAlert(true);
      })
      .finally(() => {
        // we're done, hide the spinner
        setLoading(false);
      });
  }

  return (
    <Container style={{ padding: 50 }}>
      {showAlert ? (
        <Alert variant="danger" onClose={() => setShowAlert(false)} dismissible>
          <Alert.Heading>Fout!</Alert.Heading>
          <p>Stad niet gevonden in database.</p>
        </Alert>
      ) : null}
      <h1>Het weer</h1>
      <br></br>
      <Row className="justify-content-center">
        <Form className="d-flex">
          <Form.Group className="sm" controlId="formBasicEmail">
            <Form.Control
              style={{ paddingRight: 10 }}
              placeholder="Stad"
              value={searchInput}
              onChange={(e) => setSearchInput(e.target.value)}
            />
          </Form.Group>
          <br></br>
          <Button
            variant="primary"
            // disable submit button if search input is empty
            disabled={searchInput === ""}
            onClick={search}
          >
            Verstuur
          </Button>
        </Form>
      </Row>
      <br></br>
      {loading ? (
        // while loading === true, show spinner instead of city card
        <Spinner animation="border" role="status" />
      ) : (
        city && (
          // if a city is present, show card object with city
          <Row>
            <Card style={{ width: 200, margin: 20 }}>
              <Card.Body>
                <h1>{weatherIcon(city.weer)}</h1>
                <Card.Title>{city.plaats}</Card.Title>
                <Card.Text>
                  Het is {city.temperatuur} graden en {city.weer}.
                </Card.Text>
              </Card.Body>
            </Card>
          </Row>
        )
      )}
    </Container>
  );
}

export default App;
